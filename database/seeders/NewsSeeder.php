<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\News;
use App\Models\Category;
use Faker\Factory as Faker;

class NewsSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        $categories = Category::all();

        foreach ($categories as $category) {
            for ($i = 0; $i < 5; $i++) {
                News::create([
                    'category_id' => $category->id,
                    'title' => $faker->sentence,
                    'content' => $faker->paragraph,
                    'preview' => $faker->sentence,
                    'publication_date' => $faker->dateTimeThisMonth(),
                    'views' => $faker->numberBetween(0, 20),
                ]);
            }
        }
    }
}

