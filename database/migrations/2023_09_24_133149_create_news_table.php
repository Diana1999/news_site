<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id'); // Внешний ключ на категорию
            $table->string('title'); // Заголовок новости
            $table->text('content'); // Текст новости
            $table->text('preview'); // Превью новости
            $table->dateTime('publication_date')->nullable(); // Дата публикации (с возможностью NULL)
            $table->integer('views')->default(0); // Количество просмотров (по умолчанию 0)

            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('news');
    }
};
