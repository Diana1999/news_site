<?php

namespace App\Http\Controllers;

use App\Http\Repositories\NewsRepo;
use App\Http\Requests\NewsRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Auth;


class NewsController extends Controller
{
    public function __construct(NewsRepo $repo) {
        $this->repo = $repo;
    }
    public function getNews()
    {
        $news = $this->repo->getNews();

        return response()->json($news);
    }

    public function getNewsById(int $id)
    {
        $news = $this->repo->getNewsById($id);

        if (!$news) {
            throw new NotFoundHttpException('Запись не найдена');
        }

        return response()->json($news);
    }

    public function createNews(NewsRequest $request)
    {
        $user = Auth::user();

        if ($user->roles->contains('name', 'admin')) {
            $request = $request->validated();
            $news = $this->repo->createNews($request);

            return response($news);
        }else{
            return response()->json(['message' => 'Доступ запрещен. Необходима роль администратора.'], 403);
        }
    }

    public function updateNews(NewsRequest $request, int $id)
    {
        $user = Auth::user();

        if ($user->roles->contains('name', 'admin')) {
            $request = $request->validated();
            $news = $this->repo->updateNews($request, $id);

            return response()->json($news);
        }else{
            return response()->json(['message' => 'Доступ запрещен. Необходима роль администратора.'], 403);
        }
    }

    public function deleteNews( int $id)
    {
        $user = Auth::user();

        if ($user->roles->contains('name', 'admin')) {
            $news = $this->repo->deleteNews( $id);

            if ($news) {
                return response()->json(['message' => 'Запись была удалена']);
            }
        } else {
            return response()->json(['message' => 'Доступ запрещен. Необходима роль администратора.'], 403);
        }

        return response()->json(['message' => 'Запись не найдена'], 404);
    }
    public function getTopNews()
    {
        $news = $this->repo->getTopNew();

        return response()->json($news);
    }
}
