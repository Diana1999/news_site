<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Repositories\UserRepo;
use App\Models\Role;

class UserController extends Controller
{
    public function __construct(UserRepo  $repo) {
        $this->repo = $repo;
    }

    public function register(UserRequest $request) {
        $request = $request->validated();

        $user = $this->repo->create($request);

        $user->roles()->attach(2);

        return response()->json($user);
    }

    public function login(UserRequest $request)
    {
        if (Auth::attempt($request->validated())) {
            $user = Auth::user();
            $token = $user->createToken('auth-token')->plainTextToken;

            return response()->json([
                'user' => $user,
                'token' => $token,
            ]);
        }

        return response()->json(['message' => 'Unauthorized'], 401);
    }

    public function updateRole(UserRequest $request)
    {
        $data = $request->validated();

        $user = $this->repo->findByEmail($data);

        if (!$user) {
            return response()->json(['message' => 'Пользователь с указанным email не найден'], 404);
        }
        $adminRole = Role::where('name', 'admin')->first();

        if (!$adminRole) {
            return response()->json(['message' => 'Роль "админа" не найдена'], 404);
        }

        $user->roles()->sync([$adminRole->id]);

        return response()->json(['message' => 'Роль пользователя успешно обновлена на "админа"']);
    }
}
