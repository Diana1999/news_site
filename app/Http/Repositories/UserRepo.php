<?php

namespace App\Http\Repositories;

use App\Models\User;

class UserRepo
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes)
    {
        return $this->model->create([
            'name' => $attributes['name'],
            'email' => $attributes['email'],
            'password' => $attributes['password'],
        ]);
    }

    public function findByEmail(array $data)
    {
        return $this->model
            ->where('email', $data['email'])
            ->first();
    }
}
