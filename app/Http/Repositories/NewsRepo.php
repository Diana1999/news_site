<?php

namespace App\Http\Repositories;

use App\Models\News;

class NewsRepo
{
    public function __construct(News $model)
    {
        $this->model = $model;
    }

    public function getNews()
    {
        $news = $this->model->orderBy('publication_date', 'asc')->get();
        return $news;
    }


    public function getNewsById( int $id) {
       $query =  $this->model
           ->where('id', $id)
           ->get();

       return $query;
    }

    public function createNews( array $data) {
        return $this->model->create($data);
    }

    public function updateNews( array $data, int $id) {
        return $this->model
            ->where('id',  $id)
            ->update($data);
    }

    public function deleteNews( int $id) {
        return $this->model
            ->where('id',  $id)
            ->delete();
    }


    public function getTopNew( ) {
        return $this->model
            ->where('views', '>', 10  )
            ->get();
    }
}
