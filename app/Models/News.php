<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $table = 'news';

    public $fillable = [
        'category_id',
        'title',
        'content',
        'preview',
        'publication_date',
        'views'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
